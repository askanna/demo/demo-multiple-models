# Demo run and compare multiple models

This is a project template that can be used to start a demo project. This template contain a Python script and Jupyter Notebook. Both doing the same, but it shows different use cases.

The `askanna.yml` contain the configuration for running the Python script and Jupyter Notebook. In AskAnna you can run
both. When you run these jobs, on the run page you can check the result. Here you will find an image that compares
the performacne of the different models. Also you can find related metrics, variables and artifact.

You can read more about this demo project in [the AskAnna documentation](http://docs.askanna.io/examples/multiple-models/).

Using the AskAnna CLI you can run the next command to create a new project in AskAnna with this template:

```shell
askanna create "Compare multiple models" --template https://gitlab.com/askanna/demo/demo-multiple-models.git --push
```
